app.controller('HomeController', function ($scope ){ 
	

	
	
});


app.controller('MovieController', function ($scope, MovieService){ 
	
    $scope.movies = MovieService.all();
	
	
});

app.controller('MovieDetailController', function ($scope, $routeParams, MovieService){ 
    
    $scope.movieId = $routeParams.movieId;

    $scope.movie = MovieService.get( $scope.movieId);
	
	
});

app.controller('BookingController', function ($scope, $routeParams, MovieService, CinemaService,BookingService){ 
    
    $scope.movieId = $routeParams.movieId;

    $scope.movie = MovieService.get( $scope.movieId);
    $scope.selectedShowTime = "$scope.movie.";
    $scope.numSeats = 2;
    $scope.numSeatsChosen = 0;
    $scope.isBooked = 0;
	$scope.bookingData = BookingService.get();
	$scope.cinema = CinemaService.get($scope.movie.cinemaId);
    
    $scope.rows = [];
	$scope.cols = [];
	
	for(var i = 0; i < $scope.cinema.rows ; i++) {
		
		$scope.rows.push(i);

	}
	
	for(var i = 0; i < $scope.cinema.cols ; i++) {
		
		$scope.cols.push(i);

    }
    

    $scope.chooseSeat = function(obj) {
		
		var row = $scope.rowChosen = obj.$parent.$index;
		var col = $scope.colChosen = obj.$index;
		
		var chosenSeat = "#seat_" + $scope.rowChosen + "_" + $scope.colChosen;
		
		if (! CinemaService.boolSeatReserved($scope.movie.cinemaId, row,col)) {
			
			if ( $(chosenSeat).hasClass("selected")) {
				$scope.rowChosen ="";
				$scope.colChosen ="";
				$(chosenSeat).removeClass("selected");
				$scope.numSeatsChosen--;
				return;
				
			}
			
			
			if ($scope.numSeatsChosen < $scope.numSeats) {
			
			//$(".seat").removeClass("selected");
			
			$(chosenSeat).addClass("selected");
			$scope.numSeatsChosen++
			}
			
		}
		
		
		
		
		
    }
    
    $scope.createBooking = function () {
		console.log("Booking is being made");
		$scope.isBooked = 1;
		$scope.bookingData.row = $scope.rowChosen
		$scope.bookingData.col = $scope.colChosen
		
		$scope.cinema.reservations.push([$scope.rowChosen,$scope.colChosen ]);
		
		//BookingService.setBooking("new booking name");
		
	}
	
});

app.controller('BookedController', function ($scope,BookingService){ 
	
	
	$scope.bookingData = BookingService.get();
	
});



