app.config(function($routeProvider) {
    
   
        $routeProvider

          // route for the home page
          .when('/', {
            templateUrl : 'app/templates/home.html',
            controller  : 'HomeController'
        })
        .when('/movies', {
            templateUrl : 'app/templates/movies.html',
            controller  : 'MovieController'
        })
        .when('/movies/:movieId', {
            templateUrl : 'app/templates/moviedetail.html',
            controller  : 'MovieDetailController'
        })

        .when('/booking/:movieId', {
            templateUrl : 'app/templates/booking.html',
            controller  : 'BookingController'
        })
        .when('/booked', {
            templateUrl : 'app/templates/booked.html',
            controller  : 'BookedController'
        })
        
});